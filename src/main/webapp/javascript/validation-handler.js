WAPP.validationHandler = function () {
    function validateExistance(inputField) {
        var actualValue         = inputField.val(),
            errorMsgContainer   = inputField.next(),
            hasValue,
            isLongEnough;

        hasValue = WAPP.validationService.required(actualValue);

        if (!hasValue) {
            errorMsgContainer.text('This field is required!');
            return false;
        }

        return true;
    }

    function hideValidationError(inputField) {
        inputField.next().text('');
    }

    function validateCityField() {
        var isValid = validateExistance($(this));

        if (isValid) {
            hideValidationError($(this));
        }
    }

//    function validateCreditCardField () {
//        var actualValue         = $(this).val(),
//            errorMsgContainer   = $(this).next(),            
//            isValid             = validateExistenceAndMinlength($(this), 16),
//            containsOnlyDigits;
//                  
//        if (isValid) {
//            containsOnlyDigits = WAPP.validationService.containsOnlyDigits(actualValue);
//
//            if (!containsOnlyDigits) {
//                errorMsgContainer.text('Yeehaa! Please numbers only!');
//            } else {
//                hideValidationError($(this));
//            }
//        }
//    }

     function handleSubmit(event) {
        event.preventDefault();

        validateCityField();

        // check whether all fields are valid, if it is, let's send it via AJAX call.
    } 


    function init() {
        $('form').on('submit', handleSubmit);
        $('#city').blur(validateCityField);
    }

    return {
        init: init
    };
}();