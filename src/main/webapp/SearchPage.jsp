<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="css/style.css">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
    	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript">
        window.WAPP = window.WAPP || {};
    </script>
    
    <!--  
    <script src="javascript/validation-service.js"></script>
    <script src="javascript/validation-handler.js"></script>
    <script src="javascript/main.js"></script>-->

</head>
<body>
	New York Temp: ${newYorkTemp} Pressure: ${newYorkPressure} Humidity: ${newYorkHumidity} <img src="${newYorkIcon}"><br>
	<h1>Search city:</h1>
	<form method="post" action="SearchServlet">
	 <label for="city">
			city
                </label>
		<input name="city" id="city" type="text">
		<div class="error-msg-container"></div><br>
		<input type="submit" value="Search" >
		<div class="error-msg-container"></div><br>
	</form>


</body>
</html>
