<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>WeatherApp Result</title>
</head>
<body>
	New York Temp: ${newYorkTemp} Pressure: ${newYorkPressure} Humidity: ${newYorkHumidity} <img src="${newYorkIcon}"><br>
	City: ${city}<br>
	Temperature: ${temp}<br>
	<img src="${icon}">
</body>
</html>