package com.epam.test;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.epam.Servlets.WeatherDataJsonParser;

public class WeatherDataJsonParserTest {
	
	private WeatherDataJsonParser jsonParser;
	private HashMap<String, String> map;
	private String inputString;
	private String actualTemp;
	private String actualIcon;
	private String actualPressure;
	private String actualHumidity;
	
	@Before
    public void setUp() {
		jsonParser= new WeatherDataJsonParser();
    }
	
	@Test
	public void JsonParserTestNormal() {
		map = new HashMap<String, String>();
		inputString = "{\"coord\":{\"lon\":-0.13,\"lat\":51.51},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"base\":\"cmc stations\",\"main\":{\"temp\":290.29,\"pressure\":1023,\"humidity\":63,\"temp_min\":289.15,\"temp_max\":292.15},\"wind\":{\"speed\":4.6,\"deg\":100},\"clouds\":{\"all\":75},\"dt\":1441798200,\"sys\":{\"type\":1,\"id\":5091,\"message\":0.0055,\"country\":\"GB\",\"sunrise\":1441776356,\"sunset\":1441823316},\"id\":2643743,\"name\":\"London\",\"cod\":200}";
		
		map = jsonParser.parser(inputString);
		
		actualTemp = map.get("temp");
		actualIcon = map.get("icon");
		actualPressure = map.get("pressure");
		actualHumidity = map.get("humidity");
		
		String expectedTemp = "290.29";
		String expectedIcon = "04d";
		String expectedPressure = "1023.0";
		String expectedHumidity = "63.0";
		
		Assert.assertEquals(expectedTemp, actualTemp);
		Assert.assertEquals(expectedIcon, actualIcon);
		Assert.assertEquals(expectedPressure, actualPressure);
		Assert.assertEquals(expectedHumidity, actualHumidity);
			
	}
	
	
}
