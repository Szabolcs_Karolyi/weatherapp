package com.epam.Servlets;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class WeatherDataJsonParser {
	public static HashMap<String, String> parser(String inputString){
		HashMap<String, String> map = new HashMap<String, String>();
		
		JSONObject jsonObject = new JSONObject(inputString);
		String temp = String.valueOf(jsonObject.getJSONObject("main").getDouble("temp"));
		String pressure = String.valueOf(jsonObject.getJSONObject("main").getDouble("pressure"));
		String humidity = String.valueOf(jsonObject.getJSONObject("main").getDouble("humidity"));
		JSONArray jsonArray = jsonObject.getJSONArray("weather");
		String icon = jsonArray.getJSONObject(0).getString("icon");
		
		map.put("temp", temp);
		map.put("icon", icon);
		map.put("pressure", pressure);
		map.put("humidity", humidity);
		
		return map;
	}
}
