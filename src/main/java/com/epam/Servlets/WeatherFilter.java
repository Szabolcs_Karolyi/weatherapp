package com.epam.Servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class WeatherFilter implements Filter {

	private FilterConfig filterCongfig;

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;

		String city = "New+York";

		String url = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric";

		UrlLoader urlLoader = new UrlLoader();
		String resultString = urlLoader.callURL(url);

		HashMap<String, String> newYorkMap = new HashMap<String, String>();
		WeatherDataJsonParser parser = new WeatherDataJsonParser();
		newYorkMap = parser.parser(resultString);

		String newYorkTemp = newYorkMap.get("temp");
		String newYorkPressure = newYorkMap.get("pressure");
		String newYorkHumidity = newYorkMap.get("humidity");

		String newYorkUrl = "http://openweathermap.org/img/w/" + newYorkMap.get("icon") + ".png";

		req.setAttribute("newYorkPressure", newYorkPressure);
		req.setAttribute("newYorkHumidity", newYorkHumidity);
		req.setAttribute("newYorkIcon", newYorkUrl);
		req.setAttribute("newYorkTemp", newYorkTemp);
		chain.doFilter(req, response);
	}

	public void init(FilterConfig config) throws ServletException {
		this.filterCongfig = config;
	}

}
