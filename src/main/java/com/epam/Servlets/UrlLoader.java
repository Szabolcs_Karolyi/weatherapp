package com.epam.Servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class UrlLoader {
	public static String callURL(String myURL) throws RuntimeException, IOException {
		StringBuilder sb = new StringBuilder();
		URLConnection urlConn = null;
		final int TIMEOUT = 60*1000;
		
		URL url = new URL(myURL);
		urlConn = url.openConnection();
		if (urlConn != null) {
			urlConn.setReadTimeout(TIMEOUT);
		}
		if (urlConn != null && urlConn.getInputStream() != null) {
			try (InputStreamReader in = new InputStreamReader(urlConn.getInputStream(), Charset.defaultCharset())) {
				BufferedReader bufferedReader = new BufferedReader(in);
				if (bufferedReader != null) {
					int cp;
					while ((cp = bufferedReader.read()) != -1) {
						sb.append((char) cp);
					}
					bufferedReader.close();
				}
			}
		}
		return sb.toString();
	}
}
