package com.epam.Servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final HttpSession session = request.getSession();
		String city;

		city = (String) session.getAttribute("city");

		if (city != null && !city.equals("")) {

			city = city.replace(" ", "+");
			String url = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric";

			String resultString = UrlLoader.callURL(url);

			HashMap<String, String> map = new HashMap<String, String>();

			map = WeatherDataJsonParser.parser(resultString);

			String imgUrl = "http://openweathermap.org/img/w/" + map.get("icon") + ".png";

			session.setAttribute("temp", map.get("temp"));
			request.setAttribute("temp", session.getAttribute("temp"));

			session.setAttribute("icon", imgUrl);
			request.setAttribute("icon", session.getAttribute("icon"));

			request.getRequestDispatcher("Result.jsp").include(request, response);
		} else {
			response.sendRedirect("SearchServlet");
		}
	}

}
